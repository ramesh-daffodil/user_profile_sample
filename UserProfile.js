import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ConfiguredRadium from '../../../../ConfiguredRadium';
import Field from './UserProfileField';
import Loader from '../../../common/Loader';
import { getUserProfile } from '../../../../actions/UserProfileActions';
import { cookiesGet } from '../../../../actions/CookieActions';
import styleBtn from '../../../../helpers/CssHelpers';

const styles = {
  title: {
    fontSize: '14px',
    fontWeight: '700',
    height: '30px',
    lineHeight: '30px',
    marginBottom: '10px',
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '6px',
    lineHeight: '30px',
  },
};

@connect(({ userProfile, user }) => ({ userProfile, user }), { cookiesGet, getUserProfile })
@ConfiguredRadium
class UserProfile extends React.Component {
  static contextTypes = {
    nls: React.PropTypes.shape({}),
  };
  componentWillMount() {
    const userProfile = this.props.userProfile;
    if (typeof userProfile.data.email === 'undefined') {
      this.props.getUserProfile({ token: this.props.cookiesGet('token') });
    }
  }

  renderFields() {
    const { userProfile: { data: userProfileData } } = this.props;
    return Object
      .keys(userProfileData)
      .map((item, key) => {
        const field = { ...userProfileData[item] };
        if (item === 'country_code') {
          field.value = userProfileData[item].countries[field.value].trim();
        }
        if (item === 'preferred_language') {
          field.value = userProfileData[item].languages[field.value];
        }
        return (
          <Field key={key} label={field.title} value={field.value} isInput={key > 5} />
        );
      });
  }

  render() {
    const {
      userProfile: { isLoading },
      user: { user, verified },
    } = this.props;
    const { nls } = this.context;
    return (
      <div>
        <div style={styles.title}>{nls.profile}</div>
        {isLoading ? <Loader /> : this.renderFields()}
        <p>{nls.contact_support}</p>
        <div style={styles.buttonWrapper}>
          <Link
            style={styleBtn.button}
            className="var_width"
            to={`/${domain_lang}/account/myaccount_userprofile/password_edit`}
            onClick={() => window.scrollTo(0, 0)}
          >
            {nls.change_password_button}
          </Link>
        </div>
      </div>
    );
  }
}

UserProfile.propTypes = {
  cookiesGet: React.PropTypes.func,
  getUserProfile: React.PropTypes.func,
  userProfile: React.PropTypes.shape({}),
  location: React.PropTypes.shape({}),
  user: React.PropTypes.shape({}),
  deposit: React.PropTypes.shape({
    info: React.PropTypes.shape({}),
  }),
};

export default UserProfile;
