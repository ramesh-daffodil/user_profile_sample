### What is this repository for? ###

A small sample code extracted from our large application and this piece of code is used to render user profile data. It first calls an action to get user token from cookies and then using this token it fires another action to get user profile data from the server. We are using following API's here

1) Redux (For single state Management).

2) Radium with inline CSS.