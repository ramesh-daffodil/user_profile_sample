import React from 'react';
import ConfiguredRadium from '../../../../ConfiguredRadium';

const styles = {
  field: {
    display: 'flex',
  },
  label: {
    width: '170px',
    margin: '5px 0 5px 4px',
    paddingRight: '8px',
    paddingTop: '3px',
    color: '#000',
    fontSize: '12px',
    lineHeight: '20px',
  },
  fieldValue: {
    padding: '6px 0 6px 8px',
    height: '11px',
    lineHeight: '11px',
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: '#a6a6a6',
    borderRadius: '3px',
    margin: '5px 0',
    width: '210px',
    color: '#444',
  },
  inputValue: {
    padding: '6px 0 6px 8px',
    borderColor: '#a6a6a6',
    width: '210px',
    fontSize: '11px',
  },
  disabled: {
    backgroundColor: '#eee',
  },
  password: {
    lineHeight: '16px',
  },
};

@ConfiguredRadium
class Field extends React.Component {
  render() {
    const { label, value, isInput } = this.props;
    return (
      <div style={styles.field}>
        <div style={styles.label}>{label}</div>

        {!isInput ?
          <div style={[styles.fieldValue, styles.disabled, label === 'Passwort' && styles.password]}>{value}</div> :
          <input
            disabled="disabled"
            defaultValue={value}
            style={[styles.fieldValue, styles.inputValue, styles.disabled]}
          />
        }

      </div>
    );
  }
}

Field.propTypes = {
  label: React.PropTypes.string,
  value: React.PropTypes.string,
  isInput: React.PropTypes.bool,
};

export default Field;
